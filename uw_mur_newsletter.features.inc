<?php
/**
 * @file
 * uw_mur_custom_module_captcha.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_mur_newsletter_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "captcha" && $api == "captcha") {
    return array("version" => "1");
  }
}
