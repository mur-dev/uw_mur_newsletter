jQuery(function() {
  /**
 * @file
 * Hide the state and province dropdowns unless they are already selected (e.g. after failing validation)
 */
  if (jQuery('#edit-country').val() != 'USA') {
    jQuery('#state-select').hide();
    jQuery('#edit-state').val('AL'); // Provide default value so form can be submitted while dropdown is hidden
  }
  if (jQuery('#edit-country').val() != 'CAN') {
    jQuery('#province-select').hide();
    jQuery('#edit-province').val('BC'); // Provide default value so form can be submitted while dropdown is hidden
  }
  if (jQuery('#edit-country').val() == '') disable_choosing_school(true);
  else disable_choosing_school(false);

  jQuery('#edit-country').change(function() {
    if (this.value == 'USA') {
      jQuery('#edit-state').val('None'); // Remove the default value
      jQuery('#state-select').show();
      jQuery('#province-select').hide();
      jQuery('#edit-province').val('BC'); // Provide default value so form can be submitted while dropdown is hidden
      disable_choosing_school(true);
    }
    else if (this.value == 'CAN') {
      jQuery('#edit-province').val('None'); // Remove the default value
      jQuery('#state-select').hide();
      jQuery('#province-select').show();
      jQuery('#edit-state').val('AL'); // Provide default value so form can be submitted while dropdown is hidden
      disable_choosing_school(true);
    }
    else {
      jQuery('#state-select').hide();
      jQuery('#edit-state').val('AL'); // Provide default value so form can be submitted while dropdown is hidden
      jQuery('#province-select').hide();
      jQuery('#edit-province').val('BC'); // Provide default value so form can be submitted while dropdown is hidden
      if (jQuery('#edit-country').val() == '') disable_choosing_school(true);
      else disable_choosing_school(false);
    }
  });

  jQuery('#edit-state').change(function() {
    if (jQuery('#edit-state').val() != '') disable_choosing_school(false);
  });

  jQuery('#edit-province').change(function() {
    if (jQuery('#edit-province').val() != '') disable_choosing_school(false);
  });
});

function disable_choosing_school(disable) {
  jQuery('#counsellor_school').prop('disabled', disable);
  if (disable) jQuery('#counsellor_school').val('Please select your country first');
  else jQuery('#counsellor_school').val('');
}
