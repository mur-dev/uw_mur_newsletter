jQuery(function() {
  /**
   * @file
   * Removes caching functionality from autocomplete
   */
  Drupal.ACDB.prototype.search_backup = Drupal.ACDB.prototype.search;
  Drupal.ACDB.prototype.search =
  function (searchString) {
    this.cache = {}; // Clear cache to prevent a caching bug
    this.search_backup(searchString);
  };
});
