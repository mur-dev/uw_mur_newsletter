# UW MUR Newsletter Module
This module allows a user to fill out a form where they can sign up for a newsletter that will be sent to their email. This newsletter contains, among other things, information about admissions, programs, and special events. There is a newsletter for students and for guidance counsellors.

[uwaterloo.ca/future-students/subscribe](https://uwaterloo.ca/future-students/subscribe)

[uwaterloo.ca/future-students/counsellors/subscribe](https://uwaterloo.ca/future-students/counsellors/subscribe)

[uwaterloo.ca/future-students/admin/config/mailing_list_landing](https://uwaterloo.ca/future-students/admin/config/mailing_list_landing)

---
---
# Functions

---
---
### function uw_mur_newsletter_menu()
This hook enables the module to register paths in order to define how URL requests are handled.

##### Returns
An associative array whose keys define paths and whose values are an associative array of properties for each path.

##### Additional Information
See the [api documentation](https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu/7.x) for this specific hook for more information.

---
---
### function uw_mur_newsletter_student_signup($form_state)
This is the actual form of the page that users will see. It is comprised of various types, including, but not limited to:
- button
- checkbox
- markup
- select
- submit
- textarea
- textfield

##### Returns
The form that is rendered and viewable by the user.

##### Additional Information
See the [form api documentation](https://api.drupal.org/api/drupal/developer%21topics%21forms_api_reference.html/7.x) for the various types, their descriptions, their properties, and their examples.

---
---
### function uw_mur_newsletter_student_signup_validate($form, &$form_state)
Validates the input of the user. If there is an issue, the form will reset and an error will appear indicating the issue to the user.

---
---
### function uw_mur_newsletter_student_signup_submit($form, &$form_state)
Submits the form and sends the data to the CRM to be stored.

---
---
### function uw_mur_newsletter_admin()
Allows for the configuration of this module by a site manager. These are the fields that can be modified in CKEditor in drupal.

##### Returns
The fields that are editable by a site manager.

---
---
### function uw_mur_newsletter_admin_submit($form, &$form_state)
Submits the changes that an admin has made to the module.

---
---
### function uw_mur_newsletter_counsellor_signup($form_state)
This is the actual form of the page that users will see. This form is for the guidance counsellors. It is comprised of various types, including, but not limited to:
- button
- checkbox
- markup
- select
- submit
- textarea
- textfield

##### Returns
The form that is rendered and viewable by the user.

##### Additional Information
See the [form api documentation](https://api.drupal.org/api/drupal/developer%21topics%21forms_api_reference.html/7.x) for the various types, their descriptions, their properties, and their examples.

---
---
### function uw_mur_newsletter_counsellor_signup_validate($form, &$form_state)
Validates the input of the user. If there is an issue, the form will reset and an error will appear indicating the issue to the user.

---
---
### function uw_mur_newsletter_counsellor_signup_submit($form, &$form_state)
Submits the form and sends the data to the CRM to be stored.

---
---
### function _schools_autocomplete($string)
This retrieves a list of schools based on what the user's input.

##### Returns
Returns the query to the form in json.

---
---
### function school_in_db($school)
This function retrieves a list of schools stored in the database.

##### Returns
Returns the list of schools.

---
---
### function uw_mur_newsletter_crm_authorization
This authorizes access to the CRM.

---
---
### function uw_mur_newsletter_crm_call($method, $parameters, $url)
This is the cURL request that transfer the data.

##### Returns
The response of the server.

---
---
### function uw_mur_newsletter_add_lead_crm($first_name, $last_name, $email, $start_year, $faculties)
Saves the submitted data to the CRM. It adds a lead and stores data such as first name, last name, and etc.

---
---
### function uw_mur_newsletter_add_contact_crm($first_name, $last_name, $email, $school, $newsletter_types)
Saves the submitted data to the CRM. It adds a contact and stores data such as first name, last name, and etc.