<?php
/**
 * @file
 * uw_mur_custom_module_captcha.captcha.inc
 */

/**
 * Implements hook_captcha_default_points().
 */
function uw_mur_newsletter_default_points() {
  $export = array();

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'uw_mur_newsletter_counsellor_signup';
  $captcha->module = 'recaptcha';
  $captcha->captcha_type = 'reCAPTCHA';
  $export['uw_mur_newsletter_counsellor_signup'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'uw_mur_newsletter_student_signup';
  $captcha->module = 'recaptcha';
  $captcha->captcha_type = 'reCAPTCHA';
  $export['uw_mur_newsletter_student_signup'] = $captcha;

  return $export;
}
